---
layout: default
title: Data
nav_exclude: true
---

# Data
{: .fs-9 .no_toc }

Feeding the machine.
{: .fs-6 .fw-300 }

[Data glossary](#data-glossary){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 } [Ad-hoc analysis requests](#adhoc-analysis){: .btn .fs-5 .mb-4 .mb-md-0 }

<details markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
- TOC
{:toc}
</details>
<hr>

# :construction: UNDER CONSTRUCTION :construction:

# Modern Data Platform

# Data Catalog

# Data Glossary

# Reporting

# Ad-hoc Analysis

# Data Culture

## Data Dives



