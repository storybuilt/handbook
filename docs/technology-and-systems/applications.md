---
layout: default
title: Applications
nav_exclude: true
---

# Applications
{: .fs-9 .no_toc }

The software we use to run the business.
{: .fs-6 .fw-300 }

[See the list](#application-catalog){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 } [Need access?](#administration){: .btn .fs-5 .mb-4 .mb-md-0 }

<details markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
- TOC
{:toc}
</details>
<hr>

# :construction: UNDER CONSTRUCTION :construction:

# Application Lifecycle Management
<br>

```mermaid!
%%{init: {'securityLevel': 'loose', 'theme':'base'}}%%
flowchart LR
subgraph Assessment
    A[Gather]
    B[Investigate]
    C[Discover]
    D[Decide]
end
subgraph Implementation
    E[Design]
    F[Build]
    G[Test]
    H[Go-Live]
end
    L[Retire]
subgraph Administration
    I[Iterate]
    J[Maintain]
    M[Educate]
    K[Revaluate]
end    
    A-->B-->C-->D-->E-->F-->G-->H-->I
    K--oA
    G-.->E
    C-.->A
    K-.........-xL
    M-->J-->K-.->I-->M
```

## Administration

Each application has at least one *product owner* outside of the technology and systems team, sometimes one per market, who is the subject matter expert for the given application. It is their responsibility to maintain expert-level product knowledge by studying documentation and attending business reviews, user groups, training, and new release webinars. Product owner assignments are recorded in the [Application Reference](https://pswrealestate.sharepoint.com/sites/Systems/Shared%20Documents/General/applications/applications-reference.xlsx?web=1).

The technology and systems team supports the application with user provisioning during initial onboarding. Ongoing **maintenance** of project-specific settings, user assignments, and new user training is the responsibility of the product owner and local department teams.

We support periodic **revaluation** of the application by conducting audits of licensing, permissions, and usage. If the application is no longer serving the needs of our business, we collaborate with the product owner on **iterations** of existing system designs and/or **education** of users on best practices/new features.

## Assessment

When education and iteration is not enough, we guide product owners through the process of assessing replacement tools. 

...defining requirements, selection criteria, due diligence, decision-makers, contract negotiations...

## Implementation

Project management guidelines

# Application Maps
<br>

```mermaid!
%%{init: {'securityLevel': 'loose', 'theme':'base'}}%%
graph LR
subgraph accounting & finance
    A(NEWSTAR)
    B(FINANCIAL MODEL)
    C(PROCORE)
    D(CONCUR)
    E(BUILDPRO)
    F(YARDI)
  subgraph reporting
    G(INFORMXL)
    H(POWER BI)
  end
    B-.->|budgets|A
    C-.->|hard cost budgets|B
    C-.->|payapps|D 
    D-.->|invoices|A
    A-.->|payments|C
    D-.->|credit cards|A
    D-.->|exployee expenses|A
    E--->|approvals|A 
    A-->|purchase orders|E
    F-.->|net income|A
    A-->|data|G
    A-->|data|H
    C-->|data|H
    H-->|actuals|B
end

```

```mermaid!
%%{init: {'securityLevel': 'loose', 'theme':'base'}}%%

graph LR
subgraph community management
subgraph channels
    E(ZUMPER)
    F(ZILLOW)
    G(APARTMENTS.COM)
    H(CALLRAIL)
    M(WEBSITE)
end
subgraph erp
subgraph accounting
    A(YARDI VOYAGER)
end
subgraph customer portal
    B(RENTCAFE)
    B2(CONDOCAFE)
    B3(COMMERCIALCAFE)
end
    B1(CRM FLEX)
end
E -..-> B1
F -.-> B1
G -.-> B1 
H -.-> B1
B1 ---> B
B2 ----> A
B3 ----> A
B -------> A
B --> D 
B --> C 
D --> B 
C --> B
M -.-> B1
subgraph lease execution
    C(RESIDENT CHECK)
    D(BLUEMOON)
end
end
```

# Application Catalog
The [Application Reference](https://pswrealestate.sharepoint.com/sites/Systems/Shared%20Documents/General/applications/applications-reference.xlsx?web=1) contains detailed information about each application - including product owner assignments, integrations, contract terms, and audit status.

Application Playbooks are process guides with detailed steps, screenshots, and best practices - and are owned by the department teams who own the process.


## Newstar
Our core accounting system for development projects, our purchasing system for residential construction, and inventory management for sellable product.

**Departments:** Accounting, Purchasing

**How to get access:**

**Training and support:**

**Internal Playbooks:**

## Yardi
The accounting system for income-producing assets and association management.

**Departments:** Accounting, Asset Management, Community Management

**How to get access:**

**Training and support:**

**Internal Playbooks:**

## Salesforce
The backbone of our customer experience. 

**Departments:** Marketing, Sales, Leasing, Warranty, Community Management

**How to get access:**

**Training and support:**

**Internal Playbooks:**

<details markdown="block">
  <summary>
    Customer Journeys
  </summary>
  {: .text-delta }

```mermaid!
%%{init: {'securityLevel': 'loose', 'theme':'base'}}%%
graph LR
    A>lead] --> |qualification|Q((groundbreakers)) --> |pre-launch activities|B1[pre-sale] 
    Q -->  |intro|B2[pre-lease] 
    Q -->  B3[pre-subscription]
    subgraph sale
    B1 --> |appointment|C1{sale?} --> |yes|D1(post-sale) 
    D1 --> |design appointment<br>monthly updates<br>pre-drywall<br>new home presentation<br>home delivery<br>closing|E1(post-close)
    J1(special projects)
    E1 --> |portal|F1(warranty) 
    E1 --> |post-close survey|G1(11-month) 
    E1 --> |portal|H1(maintenance)
    F1 --> I1[resale]
    G1 --> |soft touchpoints|I1[resale]
    H1 --> I1[resale]
    I1 -.-> |next chapter|B1
    end
    subgraph rent
    G2(make-ready)
    B2 -->  |appointment|C2{rent?} 
    C2 --> |yes|E2(maintenance)-->F2[renewal]
    F2 -.-> C2
    end
    subgraph invest
    B3 -->  |appointment|C3{invest?}
    C3 -----> |yes|E3(reallocation)
    E3 -.-> C3
    end
    C1-.->|no|Drip((drip))
    C2-.->|no|Drip((drip))
    C3-.->|no|Drip((drip))
    F2-.->B1
```
</details>

<details markdown="block">
  <summary>
    Complex Relationships
  </summary>
  {: .text-delta }

```mermaid!
%%{init: {'securityLevel': 'loose', 'theme':'base'}}%%
flowchart LR

A1(Maria Garcia) o--o B1(Maria Garcia)
A1 -.- |referrer of|B3
A1 --- |owner of|J1

A2(Jane Doe) o--o B2(Jane Doe)
A2 --- |member of|C2
C2 --- |investor in|G1
A2 --- |owner of|J2
A2 --- |member of|D2

A3(Sally Smith) o--o B3(Sally Smith)
A3 --- |tenant of|J2
A3 -.- |tenant of|B2
A3 --- |buyer of|J3
A3 --- |member of|D1

A4(John Smith) o--o B4(John Smith)
A4 --- |investor in|G2
A4 --- |buyer of|J3
A4 ----- |member of|D1

A5(David Adams) o--o B5(David Adams)
A5 -..- |realtor for|B1

subgraph companies
C2(ABC Ventures, LLC)
end

G2(StoryBuilt Class A)
G1(Texas Communities) --- H2 & H1

H1(Austin) 
H1 --- I3(Frank West)
H1 --- I1(Lucy)

H2(Dallas)
H2 --- I2(Meridian)

I2 --- J1(Unit 5)
I3 --- J2(Unit 24)
I1 --- J3(Unit 8)
   
subgraph people
B1
B2
B3
B4
B5
end

subgraph markets
H1
H2
subgraph communities
I1
I3
I2
subgraph units
J1
J2
J3
end
end
end

subgraph contacts
A1
A2
A3
A4
A5
end

subgraph funds
G1
G2
end

subgraph groups
D1(Smith Family)
D2(Aaron's Buddies)
end

subgraph funds
G1
G2
end

subgraph accounts
people
companies
funds
groups
markets
communities
units
end

style accounts fill:none,stroke:#000,stroke-width:2px, stroke-dasharray: 5 5
linkStyle default stroke-width:2px,fill:none,stroke:lightgrey;

c((campaigns)) ---> |reach out to|contacts
c --> |based on data from|accounts
```
</details>

## Procore

## BuildPro

## Microsoft Project

## AutoDesk

## Adobe

## Monday.com

## Outlook

## Teams

## SharePoint

## informXL

## Power BI

## Planner

## OneNote

## AtlasX

## CoStar

## RealPage

## Bluebeam

## Bentley Systems

## CallRail

## DocuSign

## GetFeedback

## Juniper Square

## Lasso CRM

## Concur

<details markdown="block">
  <summary>
    Invoice Management
  </summary>
  {: .text-delta }

```mermaid!
flowchart LR
    subgraph submitters
    A[create new invoice and submit]
    B[request new vendor]
    C[edit and resubmit]
    end
    subgraph approvers
    E[review invoices]
    F[send back to submitter with comment]
    G[approve and send to accounting]
    H[approve and send to another approver]
    end
    subgraph accounting
    N[review vendor requests]
    O[match to existing vendor in concur]
    P[send back to submitter with comment]
    Q[input supplier into newstar]
    R[save w9]
    S[approve vendor in concur]
    I[review invoices]
    K[input invoice into newstar]
    L[save backup]
    M[approve invoice in concur]
    end

    A-.->B-.->N-.->Q-.->R-.->S
    A==>E==>G==>I==>K==>L==>M
    E-.->F-.->C-.->E
    S-.->I
    N-.->O-.->I 
    N-.->P
    I-.->P-.->C
    E-.->H-.->E
```      

## SketchUp

<hr>

<details markdown="block">
  <summary>
    Retired Applications
  </summary>
  {: .text-delta }

  **ADP**

  **FieldLens**

  **BuildTopia**

  **G-Suite**

</details>

# Shadow IT

Our philosophy and guidance on rogue applications. :sunglasses:
