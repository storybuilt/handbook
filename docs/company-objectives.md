---
layout: default
title: Company Objectives
nav_order: 2
---

# Company Objectives
{: .fs-9 .no_toc }

The things we seek.
{: .fs-6 .fw-300 }

<hr>

# :construction: UNDER CONSTRUCTION :construction:

# What are OKRs and KPIs?
<br>

**`Company Objectives`** are determined by company management for each fiscal year.

**`Department Objectives`** are determined by deparment leads for each quarter to align with company objectives.

**`Key Results`** are the planned projects that will contribute to objectives.

**`Tasks`** are the major milestones for planned projects.

**`Performance Indicators`** are quantitative measurements clearly defined and tracked systematically to gauge performance.

**`Key Performance Indicators`** are a subset of performance indicators that track success or failure of speific objectives.

<br>
```mermaid!
graph TD
    A>Company Objective] --> B[Deparment Objective 1]
    B --> C((Key<br>Result 1a))
    B --> D((Key<br>Result 1b))
    B -.- E{KPI A}
    A -.- F{KPI C}
    B -.- I{KPI B}
    B -.- F
    C --> G(Task 1)
    C --> H(Task 2)
```
<br>

# Guidelines

objectives should be **SMART** goals

objectives should be **stretch** goals

use imperative sentences

no daily or recurring maintenance activities

no more than three to five key results per objective

each objective should be tied to at least one KPI

leading indicators vs lagging indicators

# Format

## Objective 1: *Title*

*Select a one sentence description of the objective.*

<details markdown="block">
  <summary>Key Results</summary>

### `KR 1a` Select a one sentence description of the key result. 

- [ ] Select a one sentence description of the first task or milestone.
- [ ] Select a one sentence description of the second task or milestone.
- [ ] Select a one sentence description of the third task or milestone.

*`kpi tied to objective`*
</details>

# Authors

Who is responsible for creating and updating?

# Timeline

How long do OKRs run?

Sample timelines with milestones

# Retrospectives

Status of OKRs at the end of the period

Include commentary on KPIs

What went well? What contributed to success?

What did not go well? What obstacles were encountered?

What can we do better next time?

Feels + Open Questions

Emotions, mindsets, areas of confusion, and opportunities to consider.








