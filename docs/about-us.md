---
layout: default
title: About Us
nav_order: 1
---

# Who We Are

StoryBuilt is committed to building inclusive communities that encourage, support, and celebrate diversity.
{: .fs-6 .fw-300 }

# Our Vision

StoryBuilt’s vision is to enrich people’s living experiences in the cities we love through expertly crafted and connected communities.
{: .fs-6 .fw-300 }

# Our Values

WE ARE:
{: .fs-6 .fw-300 }

**Built on Community:** 

We are internally and externally driven to be connected to our people.

**Built on Challenging Norms:** 

We take risks, think outside of the box, and are unconventional.

**Built on Teamwork:** 

Collaboration is the core of our business. We encourage clear communication and transparency.

**Built on Ownership:** 

We’re all owners. We take pride in our individual and company success.

# Our Strategy

smart land acquisitions
{: .fs-6 .fw-300 }

intentional design & development
{: .fs-6 .fw-300 }

strategic partnerships
{: .fs-6 .fw-300 }

vertically integrated business model
{: .fs-6 .fw-300 }

commitment to building the brand in desirable cities
{: .fs-6 .fw-300 }

# Org Structure
