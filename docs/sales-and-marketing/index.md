---
layout: default
title: Sales & Marketing
nav_order: 9
permalink: /docs/sales-and-markeing
---

# Sales & Marketing
{: .fs-9 .no_toc }

Make us shine.
{: .fs-6 .fw-300 }

[How to Reach Us](#how-to-reach-us){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 } [Objectives and Key Results](#objectives--key-results-okrs){: .btn .fs-5 .mb-4 .mb-md-0 }

<details markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
- TOC
{:toc}
</details>
<hr>

# :construction: UNDER CONSTRUCTION :construction:

# How We Work

# How to Reach Us

# Key Performance Indicators (KPIs)

# Objectives & Key Results (OKRs)
