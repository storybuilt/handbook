---
layout: default
title: Accounting & Finance
nav_order: 5
permalink: /docs/accounting-and-finance
---

# Accounting & Finance
{: .fs-9 .no_toc }

Making it count.
{: .fs-6 .fw-300 }

[How to Reach Us](#how-to-reach-us){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 } [Objectives and Key Results](#objectives--key-results-okrs){: .btn .fs-5 .mb-4 .mb-md-0 }

<details markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
- TOC
{:toc}
</details>
<hr>

# :construction: UNDER CONSTRUCTION :construction:

# How We Work
<br>

```mermaid!
flowchart LR
    A((Accounting & Finance))
    B[AP]
    C[AR]
    D[Treasury]
    E[Financial Reporting]
    F[Audit]
    G[Investor Relations]
    H[Lenders]
    I[Capital Markets]
    J[Project Accounting]
    K[Retail Investors]
    L[FP&A]
    M[Corporate Development]
    N[Joint Ventures]
    O[Investor Services]
    P[M&A]
    Q[Investments]
    R[Fixed Assets]
    T[Associations]
    U[Tax]
    V[Developments]

    I-->H & K & N
    A-->D & E & J & I & M
    
    K-->G & O
    E-->F & U & L
    M-->P & Q
    J-->R & T & V
    D-->B & C

    D-.-E-.-I-.-M-.-J

    style D fill:#ccf
    style E fill:#ccf
    style I fill:#ccf
    style M fill:#ccf
    style J fill:#ccf
```
    

# How to Reach Us

# Key Performance Indicators (KPIs)

# Objectives & Key Results (OKRs)
