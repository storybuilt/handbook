---
layout: default
title: Acquisitions & Development
nav_order: 6
permalink: /docs/acquisitions-and-development
---

# Acquisitions & Development
{: .fs-9 .no_toc }

Building confidence.
{: .fs-6 .fw-300 }

[How to Reach Us](#how-to-reach-us){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 } [Objectives and Key Results](#objectives--key-results-okrs){: .btn .fs-5 .mb-4 .mb-md-0 }

<details markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
- TOC
{:toc}
</details>
<hr>

# :construction: UNDER CONSTRUCTION :construction:

# How We Work

# How to Reach Us

# Key Performance Indicators (KPIs)

# Objectives & Key Results (OKRs)


