---
layout: default
title: Technology & Systems
nav_order: 4
---

# Technology & Systems
{: .fs-9 .no_toc }

Kick the tires and light the fires.
{: .fs-6 .fw-300 }

[How to Reach Us](#how-to-reach-us){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 } [Objectives and Key Results](#objectives--key-results-okrs){: .btn .fs-5 .mb-4 .mb-md-0 }

<details markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
- TOC
{:toc}
</details>
<hr>
# How We Work

We organize our work into the following divisions:

[Applications](#applications){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 } [Data](#data){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 } [IT](#it){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }

We use a board to track key results, issues, and enhancement requests:

[Technology Planner](https://tasks.office.com/storybuilt.com/Home/PlanViews/VPI-rDJHWEiXrtitmuwzPmUAAwKv?Type=PlanLink&Channel=Link&CreatedTime=637812474952310000){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 } [Submit a request](https://forms.office.com/r/fk241P9zMn){: .btn .fs-5 .mb-4 .mb-md-0 }

We have one recurring bi-weekly meeting for coaching and progress updates, and one recurring quarterly meeting for planning and performance reviews.

We are flex-first and aim for one day minimum on site each week. While most of our communication and work is asyncronous, we must be available for triage during core business hours.

We work with people at varying levels in all departments across the company on technology projects and business process improvements.

```mermaid!
classDiagram
    direction TD
    Acq and Dev *-- Technology
        Acq and Dev : Acquisitions Analyst
        Acq and Dev : Development Analyst
        Acq and Dev : AtlasX Product Owner
        Acq and Dev : CoStar Product Owner
    Design *-- Technology
        Design : Technical Director
        Design : Innovation Specialist
        Design : AutoDesk Product Owner
    Construction *-- Technology
        Construction : Project Manager
        Construction : Production Manager
        Construction : Quality Director
        Construction : BuildPro Product Owner
        Construction : Procore Product Owner
    Accounting and Finance *-- Technology
        Accounting and Finance : Controller
        Accounting and Finance : Accounting Manager
        Accounting and Finance : Investor Relations Manager
        Accounting and Finance : FPandA Manager
        Accounting and Finance : Newstar Enterprise Product Owner
        Accounting and Finance : informXL Product Owner
        Accounting and Finance : Concur Product Owner
    Sales and Marketing *-- Technology
        Sales and Marketing : Marketing Director
        Sales and Marketing : Digital Manager
        Sales and Marketing : CX Director
        Sales and Marketing : Sales Manager
        Sales and Marketing : Sales Cloud Product Owner
        Sales and Marketing : Marketing Cloud Product Owner
        Sales and Marketing : monday.com Product Owner   
        Sales and Marketing : Adobe Product Owner  
        Sales and Marketing : Newstar Sales Product Owner
        Sales and Marketing : CallRail Product Owner
    Community Management *-- Technology
        Community Management : Community Manager 
        Community Management : Warranty Manager
        Community Management : Service Cloud Product Owner
        Community Management : Yardi Product Owner
    People *-- Technology
        People : Operations Manager
        People : Recruiting Specialist
        People : UKG Product Owner
        Technology : Systems Administrator
        Technology : Business Operations and Systems Manager
```


## Applications

We are accountable for all business applications through the entire application lifecycle - including assessment, implementation, and ongoing support. We act as consultants to dispersed product owners who are the subject matter experts and manage ongoing administration.

[Applications Handbook](applications){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 } [Need access?](https://forms.office.com/r/fk241P9zMn){: .btn .fs-5 .mb-4 .mb-md-0 }

## Data

We deliver data products to all business functions - including self-serve reporting, performance metrics, and adhoc analysis. We manage data integrity by actively encouraging data stewardship, maintaining a common data model, and integrating data between disparate systems of record.

[Data Handbook](data){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 } [Need data?](https://forms.office.com/r/fk241P9zMn){: .btn .fs-5 .mb-4 .mb-md-0 }

## IT

We enable our users by making sure they have the right software and hardware tools for the job. We are proactive with user education - teaching best practices related to security, documents, productivity, and communications. We provide technical expertise to automate and improve business processes.

[IT Handbook](it){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 } [Need hardware?](https://forms.office.com/Pages/ResponsePage.aspx?Host=Teams&lang=%7Blocale%7D&groupId=%7BgroupId%7D&tid=%7Btid%7D&teamsTheme=%7Btheme%7D&upn=%7Bupn%7D&id=YQov_CrV5kKJb3dCPZvN_siWWVETe51Nkz548-R1Lv5UOFFCTElJRUlFSkVUTFNSUEFQNkZOQlY3MiQlQCN0PWcu){: .btn .fs-5 .mb-4 .mb-md-0 }

## Roles
<br>

```mermaid!
%%{init: {'securityLevel': 'loose'}}%%
flowchart LR
    K[Applications Intern] --> A[Applications Specialist] --> B[Applications Manager]
    C[Data Intern] --> D[Data Analyst] --> E[Data Engineer]
    F[IT Intern] --> G[Systems Administrator] --> H[IT Manager]
    H -.-> I[Business Operations and Systems Manager]
    B -.-> I
    E -.-> I
    
style G fill:#ccf
style I fill:#ccf
click G "#systems-administrator"
click I "#business-operations-and-systems-manager"
```
<br>

### Systems Administrator

*Support information systems and technology efforts across all business functions.*

<details markdown="block">
  <summary>Responsibilities</summary>
  
- Help internal users troubleshoot issues with IT-related business processes.  
- Provide references for users by writing and maintaining user documentation for IT-related business processes. 
- Act as liaison between internal users and external IT support teams.  
- Maintain expert-level knowledge for core IT infrastructure by attending training webinars, studying knowledge base documentation, and keeping up with new product releases.  
- Facilitate IT onboarding and offboarding processes including initial user-administration, hardware asset management, and initial orientation.  
- Provide regular training for internal users on general IT topics including document management, communication tools, and cybersecurity.  
- Setup and maintain telecommunication utility accounts for internal users and company offices.  
- Support cloud-based document management by encouraging best practices, managing access permissions, and providing retrieval/recovery services.  
- Review and process invoices for external IT service and software providers, tracking against service agreements and internal budgets.  
- Manage internal projects related to core IT infrastructure.  
  
</details>

<details markdown="block">
  <summary>Requirements</summary>
  
- Bachelor’s degree in business, computer science, information technology, or a related field.  
- 5+ years in an IT support and administration role. 
  
</details>

### Business Operations and Systems Manager

*Lead efforts to streamline and scale operations across all business functions.*

<details markdown="block">
  <summary>Responsibilities</summary>
  
- Determine operational objectives by studying business functions; gathering information; evaluating output requirements and formats. 
- Select software for the company’s needs by analyzing requirements; constructing workflow charts and diagrams; studying system capabilities; writing specifications.  
- Improve systems by studying current practices; designing modifications.  
- Recommend controls by identifying problems; writing improved procedures.  
- Define project requirements by identifying project milestones, phases, and elements; forming project team; establishing project budget.  
- Monitor project progress by tracking activity; resolving problems; publishing progress reports; recommending actions.  
- Provide references for users by writing and maintaining user documentation; providing help desk support; training users.  
- Maintain user confidence and protects operations by keeping information confidential.  
- Prepare technical reports by collecting, analyzing, and summarizing information and trends.  
- Maintain professional and technical knowledge by attending educational workshops; reviewing professional publications; establishing personal networks; benchmarking state-of-the-art practices; participating in professional societies.  
- Contribute to team effort by accomplishing related results as needed.  
- Assist in the customization and adaptation of existing programs to meet users’ requirements.  
- Develop software and system related budgets.   
- Maintain safe and healthy working environment by establishing and enforcing organization standards; adhering to legal regulations. 
- Sustain information systems results by defining, delivering, and supporting information systems; auditing application of systems.  
- Assess information systems results by auditing application of systems.  
- Enhance information systems results by identifying information systems technology opportunities and developing application strategies.  
- Safeguard assets by planning and implementing disaster recovery and back-up procedures and information security and control structures.  
- Accomplishe financial objectives by determining service level required; preparing an annual budget; scheduling expenditures; analyzing variances; initiating corrective action.  
  
</details>

<details markdown="block">
  <summary>Requirements</summary>
  
- Bachelor's degree in business, computer science, information technology, or other computer related discipline. Master’s degree in Business Information Systems or MBA preferred. 
- 10+ years’ experience in managing and leading operations and technology.  
- 5+ years of system design and implementation leadership experience consisting of direct and indirect people management as well as technical contribution.  
- 3+ years’ experience in managing ERP functional teams in an enterprise environment.    
- Demonstrate leadership skills and ability to lead culture change within groups and across the organization.  
- This person will have an excellent ability to provide clarity out of a sea of information, have strong software design and architecture skills.   
- Be comfortable communicating with management and employees at all levels. 
  
</details>

# How to Reach Us

If it's an IT issue, you almost always need to start with [Terminal B](#it-helpdesk). If it's related to applications/data, or if it's an escalated IT issue - we're going to take care of you. The best way to reach us is via [Teams chat](https://teams.microsoft.com/l/chat/0/0?users=technology@storybuilt.com).


## IT Helpdesk

Call if it's urgent. Otherwise, send an email.

[(512) 877-8350​](tel:5128778350​){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 } [support@terminalb.com](mailto:support@terminalb.com){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 } [Service Level Targets](https://www.terminalb.com/client/sla/){: .btn .fs-5 .mb-4 .mb-md-0 }


## Teams and Channels
<br>
[**StoryBuilt Global/Support and Resources**](https://teams.microsoft.com/l/channel/19%3a4d10e59509aa4d21901bc1ed0fdcb63a%40thread.skype/Support%2520and%2520Resources?groupId=515996c8-7b13-4d9d-933e-78f3e4752efe&tenantId=fc2f0a61-d52a-42e6-896f-77423d9bcdfe)

This is our org-wide channel where we make IT related announcements like system updates, outages, and user education. We also host common request forms and reference materials here as channel tabs.

[**StoryBuilt Global/Reporting**](https://teams.microsoft.com/l/channel/19%3aa9b7b2ca60a149878745e601e6b5c6d3%40thread.skype/Reporting?groupId=515996c8-7b13-4d9d-933e-78f3e4752efe&tenantId=fc2f0a61-d52a-42e6-896f-77423d9bcdfe)

This is an org-wide channel where we host company-wide reports for quick reference.

[**Technology/General**](https://teams.microsoft.com/l/channel/19%3ab7d0ac73963e45a88702bf6d310e2ace%40thread.skype/General?groupId=5a779564-3b53-4993-8136-79aacb189588&tenantId=fc2f0a61-d52a-42e6-896f-77423d9bcdfe)

This is our department team/channel. We host our shared documents here - including vendor contracts, reference material, technology project documentation, and file-based tools.

## Office Hours

TBD


# Key Performance Indicators (KPIs)

automated tracking &nbsp; :heavy_check_mark: &nbsp;
manual tracking &nbsp; :pencil2: &nbsp;
no tracking &nbsp; :question:


## Technology & Systems KPIs
These are the metrics we own.
<hr>

**`technology spend (TTM)`** is the total invoiced amount for hardware, software, and third-party technology professional services over the trailing 12 months.
&nbsp; :heavy_check_mark:

*`technology spend as % of total spend (TTM)`*
&nbsp; :heavy_check_mark:

*`technology spend as % of corporate spend (TTM)`*
&nbsp; :heavy_check_mark:

*`technology spend per employee (TTM)`*
&nbsp; :pencil2:

*`technology spend by department (TTM)`*
&nbsp; :pencil2:

*`technology spend vs plan (annual)`*
&nbsp; :question:

*`technology spend by type (% invoice/credit/po)`*
&nbsp; :pencil2:

<hr>

**`technology key results completed (TTM)`** is the total number of tasks marked complete with the `key result` label on the [Technology Planner](https://tasks.office.com/storybuilt.com/en-US/Home/Planner/#/plantaskboard?groupId=5a779564-3b53-4993-8136-79aacb189588&planId=VPI-rDJHWEiXrtitmuwzPmUAAwKv) over the trailing 12 months.
&nbsp; :pencil2:

*`technology key results completed (by quarter)`*
&nbsp; :pencil2:

*`technology key results completed (by department)`*
&nbsp; :pencil2:

<hr>

**`technology user satisfaction score (TTM)`** is the average score response (1-10) on all technology user satisfaction surveys over the trailing 12 months.
&nbsp; :question:

*`technology user satisfaction score (onboarding)`*
&nbsp; :question:

*`technology user satisfaction score (helpdesk)`*
&nbsp; :question:

<hr>

**`helpdesk tickets per employee per month (TTM)`** is the average number of tickets submitted to third party help desk per employee over the trailing 12 months.
&nbsp; :pencil2:

<hr>

**`self-service data unique users per month (TTM)`** is the average number of unique users per month over the trailing 12 months.
&nbsp; :pencil2:

*`self-service data report views per month - pro (TTM)`*
&nbsp; :pencil2:

*`self-service data report views per month - free (TTM)`*
&nbsp; :question:

<hr>

**`Microsoft Secure Score`** is a composite index of an organization's security posture based on feature settings and risk awareness.
&nbsp; :pencil2:

*`phishing simulation compromised users`*
&nbsp; :pencil2:

<hr>

**`Microsoft Productivity Score`** is a composite index of people and technology based on user behaviors related to meetings, content collaboration, and communication - and the health of your hardware/software/network infrastructure.
&nbsp; :pencil2:

<hr>

**`Microsoft Compliance Score`** is a composite index of data protection risks and controls based on exisiting configurations, exposed content, and risk awareness.
&nbsp; :pencil2:

<hr>

**`training/support talks per month (TTM)`** is the number of open-invite training and/or support events hosted by the technology and systems team over the trailing 12 months.
&nbsp; :pencil2:

<hr>

**`days since last audit (by application)`** is the number of days ince the last time we audited an application for licensing, permissions, and usage.
&nbsp; :question:

<hr>


## Shared KPIs
There are the metrics owned by other departments.
<hr>

**`revenue (TTM)`**
&nbsp; :heavy_check_mark:

**`profit (TTM)`**
&nbsp; :heavy_check_mark:

**`total spend (TTM)`**
&nbsp; :heavy_check_mark:

**`corporate spend (TTM)`**
&nbsp; :heavy_check_mark:

**`average monthly employee headcount (TTM)`**
&nbsp; :pencil2:

**`onboarding user satisfaction score (TTM)`**
&nbsp; :pencil2:

**`under/over budget COS estimates (TTM)`**
&nbsp; :pencil2:

**`average accounting close cycle (TTM)`**
&nbsp; :pencil2:

**`manual journal entry transactions (TTM)`**
&nbsp; :pencil2:


# Objectives & Key Results (OKRs)

## Objective 1: _Customers_

*Continue implementation of best-in-class customer database and integrations tools to set foundation for improved customer experience.*

<details markdown="block">
  <summary>Key Results</summary>
  {: .text-delta }

### `KR 1-1` Complete Salesforce build.

- [ ] demonstrate sales cloud to key users
- [ ] demonstrate service and community cloud to key users
- [ ] demonstrate datorama and marketing cloud to key users
- [ ] demonstrate integrations via mulesoft
- [ ] complete user testing
- [ ] complete user training

### `KR 1-2` Complete MuleSoft design and build.

- [ ] establish connection to yardi data
- [ ] provide final mapping tables
- [ ] build APIs
- [ ] demonstrate integrations with salesforce
- [ ] complete user testing
- [ ] complete user training

### `KR 1-3` Integrate Yardi database into existing analytics.

- [ ] investigate yardi database backup and restore to storybuilt data warehouse
- [ ] investigate yardi webscraping to storybuilt data warehouse
- [ ] integrate yardi tables into storybuilt views

</details>

## Objective 2: _Accounting_

*Work with new Accounting Systems Manager to improve internal user experience by establishing processes for ongoing administration fo newly implemented tools and providing a framework for more granular cost tracking.*

<details markdown="block">
  <summary>Key Results</summary>
  {: .text-delta }

### `KR 2-1` Establish processes for manual Concur/Newstar integrations.

- [ ] import approved invoices from Concur into Newstar
- [ ] import payment data from Newstar into Concur
- [ ] import vendors from Newstar into Concur
- [ ] import expense types from Newstar into Concur
- [ ] import projects/units from Newstar into Concur
- [ ] update approval workflows in Concur

### `KR 2-2` Update major code list to add more detail and make less redundant.

- [ ] finalize list (consultants, warranty, technology, marketing)
- [ ] update published reference doc
- [ ] update Major Codes in Newstar
- [ ] update in Procore
- [ ] update in financial models
- [ ] update in concur
- [ ] check GL Job Interface in Newstar
- [ ] update Cost Allocation rules in Newstar
- [ ] check COS Processing rules
- [ ] provide instructions on how to transition budgets to new codes

### `KR 2-3` Retire Wrike software platform.

- [ ] move all remaining forms to MS Forms / Planner
- [ ] retrieve and archive all relevant data from wrike
- [ ] communicate to internal users
- [ ] communicate non-renewal to wrike

</details>

## Objective 3: _Employees_

*Reinforce and streamline new hire and existing employee processes to make more consistent.*

<details markdown="block">
  <summary>Key Results</summary>
  {: .text-delta }

### `KR 3-1` Integrate UKG software with active directory.

- [ ] find suitable AD integration vendor
- [ ] connect AD to UKG
- [ ] establish processes for new user creation and maintenance 

### `KR 3-2` Create IT orientation documentation and resources.

- [ ] create IT orientation topic videos
- [ ] publish

### `KR 3-3` Update employee incentive form and reestablish process.

- [ ] create employee incentive form
- [ ] develop and test submission process in Power Automate
- [ ] deploy

### `KR 3-4` Implement CDW for hardware provisioning.

- [ ] send base image to CDW for duplication
- [ ] review documentation, contract, and processes
- [ ] test purchase
- [ ] implement into new processes with Terminal B

</details>

## Objective 4: _Infrastructure_

*Explore new tools that will benefit multiple facets of technology infrastructure.*

<details markdown="block">
  <summary>Key Results</summary>
  {: .text-delta }

### `KR 4-1` Upgrade Mist office audio/video infrastructure.

- [ ] order and install hardware
- [ ] test functionality
- [ ] create documentation

### `KR 4-2` Upgrade Bluebeam licenses for simplified administration.

- [ ] receive proposal
- [ ] create before/after cost analysis
- [ ] approve and upgrade

### `KR 4-3` Evaluate GPU accelerated workspaces.

- [ ] implement for test users 
- [ ] run stress tests with Unreal / Revit
- [ ] reevaluate way forward based on testing

</details>


# Restrospectives

## 2022Q1

## Objective 1: *Framework*

*Increase throughput by establishing department structure and performance framework.*

*`key results completed (by quarter)`* = 2

We used this quarter to experiment with a new process for creating goals and tracking completion. Many of the goals we set for ourselves this quarter will be completed within the first few weeks of Q2. We are confident in this new process and plan for this number to increase throughout the rest of the year.

<details  markdown="block">
  <summary>Key Results</summary>
  {: .text-delta }

### `KR 1a` Establish general technology handbook and publish.

We used this quarter to experiment with a new process for creating goals and tracking completion. Many of the goals we set for ourselves this quarter will be completed within the first few weeks of Q2. We are confident in this new process and plan for this number to increase throughout the rest of the year.
We built the framework for a handbook website using [Jekyll](https://jekyllrb.com/), [Just the Docs](https://github.com/just-the-docs/just-the-docs), and [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/). The inspiration for building this type of handbook site is the [GitLab Handbook](https://about.gitlab.com/handbook/).

The [Technology and Systems department page](https://storybuilt.gitlab.io/handbook/docs/technology-and-systems/) is complete and can be used as a reference for other departments. The next step will be to build out subdepartment pages (i.e., Applications, Data, IT) and other general reference pages (i.e., Handbook Home, About Us, Company Objectives). 

There is a concern about ongoing maintenance with this approach since our users are not experienced in git-based version control or markdown documentation. We may explore an alternative approach utilizing SharePoint for familiarity, ease of managing content, and built-in integration with the Microsoft ecosystem.

</details>

## Objective 2: *Customers*

*Improve customer experience by upgrading customer-centric applications.*

<details  markdown="block">
  <summary>Key Results</summary>
  {: .text-delta }

### `KR 2a` Establish requirements for Salesforce CRM and MuleSoft iPaaS.

We completed in-depth discovery sessions for Salesforce with members of the sales, marketing, leasing, warranty, maintenance, community, and capital teams. The following technical artifacts were produced to aid in the build phase:

[Customer Journeys](https://mermaid.ink/svg/eyJjb2RlIjoiJSV7aW5pdDogeydzZWN1cml0eUxldmVsJzogJ2xvb3NlJywgJ3RoZW1lJzonYmFzZSd9fSUlXG5ncmFwaCBMUlxuQT5sZWFkXSAtLT4gfHF1YWxpZmljYXRpb258USgoZ3JvdW5kYnJlYWtlcnMpKSAtLT4gfHByZS1sYXVuY2ggYWN0aXZpdGllc3xCMVtwcmUtc2FsZV1cblEgLS0-ICB8aW50cm98QjJbcHJlLWxlYXNlXVxuUSAtLT4gIEIzW3ByZS1zdWJzY3JpcHRpb25dXG5zdWJncmFwaCBzYWxlXG5CMSAtLT4gfGFwcG9pbnRtZW50fEMxe3NhbGU_fSAtLT4gfHllc3xEMShwb3N0LXNhbGUpXG5EMSAtLT4gfGRlc2lnbiBhcHBvaW50bWVudDxicj5tb250aGx5IHVwZGF0ZXM8YnI-cHJlLWRyeXdhbGw8YnI-bmV3IGhvbWUgcHJlc2VudGF0aW9uPGJyPmhvbWUgZGVsaXZlcnk8YnI-Y2xvc2luZ3xFMShwb3N0LWNsb3NlKVxuSjEoc3BlY2lhbCBwcm9qZWN0cylcbkUxIC0tPiB8cG9ydGFsfEYxKHdhcnJhbnR5KVxuRTEgLS0-IHxwb3N0LWNsb3NlIHN1cnZleXxHMSgxMS1tb250aClcbkUxIC0tPiB8cG9ydGFsfEgxKG1haW50ZW5hbmNlKVxuRjEgLS0-IEkxW3Jlc2FsZV1cbkcxIC0tPiB8c29mdCB0b3VjaHBvaW50c3xJMVtyZXNhbGVdXG5IMSAtLT4gSTFbcmVzYWxlXVxuSTEgLS4tPiB8bmV4dCBjaGFwdGVyfEIxXG5lbmRcbnN1YmdyYXBoIHJlbnRcbkcyKG1ha2UtcmVhZHkpXG5CMiAtLT4gIHxhcHBvaW50bWVudHxDMntyZW50P31cbkMyIC0tPiB8eWVzfEUyKG1haW50ZW5hbmNlKS0tPkYyW3JlbmV3YWxdXG5GMiAtLi0-IEMyXG5lbmRcbnN1YmdyYXBoIGludmVzdFxuQjMgLS0-ICB8YXBwb2ludG1lbnR8QzN7aW52ZXN0P31cbkMzIC0tLS0tPiB8eWVzfEUzKHJlYWxsb2NhdGlvbilcbkUzIC0uLT4gQzNcbmVuZFxuQzEtLi0-fG5vfERyaXAoKGRyaXApKVxuQzItLi0-fG5vfERyaXAoKGRyaXApKVxuQzMtLi0-fG5vfERyaXAoKGRyaXApKVxuRjItLi0-QjEiLCJtZXJtYWlkIjpudWxsfQ)
*high-level touchpoints in the customer journey for each of the invest/rent/sale personas*

[Complex Relationships](https://mermaid.ink/svg/eyJjb2RlIjoiJSV7aW5pdDogeydzZWN1cml0eUxldmVsJzogJ2xvb3NlJywgJ3RoZW1lJzonYmFzZSd9fSUlXG5mbG93Y2hhcnQgTFJcbiUlLVxuQTEoTWFyaWEgR2FyY2lhKSBvLS1vIEIxKE1hcmlhIEdhcmNpYSlcbkExIC0uLSB8cmVmZXJyZXIgb2Z8QjNcbkExIC0tLSB8b3duZXIgb2Z8SjFcbiUlLVxuQTIoSmFuZSBEb2UpIG8tLW8gQjIoSmFuZSBEb2UpXG5BMiAtLS0gfG1lbWJlciBvZnxDMlxuQzIgLS0tIHxpbnZlc3RvciBpbnxHMVxuQTIgLS0tIHxvd25lciBvZnxKMlxuQTIgLS0tIHxtZW1iZXIgb2Z8RDJcbiUlLVxuQTMoU2FsbHkgU21pdGgpIG8tLW8gQjMoU2FsbHkgU21pdGgpXG5BMyAtLS0gfHRlbmFudCBvZnxKMlxuQTMgLS4tIHx0ZW5hbnQgb2Z8QjJcbkEzIC0tLSB8YnV5ZXIgb2Z8SjNcbkEzIC0tLSB8bWVtYmVyIG9mfEQxXG4lJS1cbkE0KEpvaG4gU21pdGgpIG8tLW8gQjQoSm9obiBTbWl0aClcbkE0IC0tLSB8aW52ZXN0b3IgaW58RzJcbkE0IC0tLSB8YnV5ZXIgb2Z8SjNcbkE0IC0tLS0tIHxtZW1iZXIgb2Z8RDFcbiUlLVxuQTUoRGF2aWQgQWRhbXMpIG8tLW8gQjUoRGF2aWQgQWRhbXMpXG5BNSAtLi4tIHxyZWFsdG9yIGZvcnxCMVxuJSUtXG5zdWJncmFwaCBjb21wYW5pZXNcbkMyKEFCQyBWZW50dXJlcywgTExDKVxuZW5kXG4lJS1cbkcyKFN0b3J5QnVpbHQgQ2xhc3MgQSlcbkcxKFRleGFzIENvbW11bml0aWVzKSAtLS0gSDIgJiBIMVxuJSUtXG5IMShBdXN0aW4pXG5IMSAtLS0gSTMoRnJhbmsgV2VzdClcbkgxIC0tLSBJMShMdWN5KVxuJSUtXG5IMihEYWxsYXMpXG5IMiAtLS0gSTIoTWVyaWRpYW4pXG4lJS1cbkkyIC0tLSBKMShVbml0IDUpXG5JMyAtLS0gSjIoVW5pdCAyNClcbkkxIC0tLSBKMyhVbml0IDgpXG4lJS1cbnN1YmdyYXBoIHBlb3BsZVxuQjFcbkIyXG5CM1xuQjRcbkI1XG5lbmRcbiUlLVxuc3ViZ3JhcGggbWFya2V0c1xuSDFcbkgyXG5zdWJncmFwaCBjb21tdW5pdGllc1xuSTFcbkkzXG5JMlxuc3ViZ3JhcGggdW5pdHNcbkoxXG5KMlxuSjNcbmVuZFxuZW5kXG5lbmRcbiUlLVxuc3ViZ3JhcGggY29udGFjdHNcbkExXG5BMlxuQTNcbkE0XG5BNVxuZW5kXG4lJS1cbnN1YmdyYXBoIGZ1bmRzXG5HMVxuRzJcbmVuZFxuJSUtXG5zdWJncmFwaCBncm91cHNcbkQxKFNtaXRoIEZhbWlseSlcbkQyKEFhcm9uJ3MgQnVkZGllcylcbmVuZFxuJSUtXG5zdWJncmFwaCBmdW5kc1xuRzFcbkcyXG5lbmRcbiUlLVxuc3ViZ3JhcGggYWNjb3VudHNcbnBlb3BsZVxuY29tcGFuaWVzXG5mdW5kc1xuZ3JvdXBzXG5tYXJrZXRzXG5jb21tdW5pdGllc1xudW5pdHNcbmVuZFxuJSUtXG5zdHlsZSBhY2NvdW50cyBmaWxsOm5vbmUsc3Ryb2tlOiMwMDAsc3Ryb2tlLXdpZHRoOjJweCwgc3Ryb2tlLWRhc2hhcnJheTogNSA1XG5saW5rU3R5bGUgZGVmYXVsdCBzdHJva2Utd2lkdGg6MnB4LGZpbGw6bm9uZSxzdHJva2U6bGlnaHRncmV5O1xuJSUtXG5jKChjYW1wYWlnbnMpKSAtLS0-IHxyZWFjaCBvdXQgdG98Y29udGFjdHNcbmMgLS0-IHxiYXNlZCBvbiBkYXRhIGZyb218YWNjb3VudHMiLCJtZXJtYWlkIjpudWxsfQ)
*hypothetical relationship scenario in Salesforce terms (contacts, accounts, opportunities, contracts, etc.)*

[Technical Design Document](https://storybuilt.quip.com/dBnTAzRIBCRo/StoryBuilt-Technical-Design-Document-EXTERNAL-FINAL)
*produced by Roycon, third-party implementation partner, and signed off on by StoryBuilt*

We initiated discovery sessions with Apisero, third-party implementation partner for MuleSoft, but decided to pause the engagement to allow the Salesforce implementation to progress. Productivity in the early sessions was limited because we didn't have a clear picture of the Salesforce object/field architecture. The sessions will continue in Q2.

</details>

## Objective 3: *Accounting*

*Reduce manual journal entries and manual system inputs by introducing controls for invoices and automated consolidations for parent/child company relationships.*

<details  markdown="block">
  <summary>Key Results</summary>
  {: .text-delta }

### `KR 3a` Implement Concur for invoice management.

We completed an initial implementation of invoice management in Concur, allowing us to begin the journey away from Wrike. The biggest win here is automated approval workflows based on projects and/or cost codes - a necessary control we didn't have before. An added benefit is using the same platform for all spend management (employee reimbursements, credit card expenses, and invoice management). This produces a better user experience and will make long-term administration easier. The following technical artifacts were produced to aid in training:

[Invoice Process](https://mermaid.ink/svg/pako:eNqVU0tPwzAM_itRzoM7k4bEY4yJAXscOw5ZamjEmpQ8OtC0_47TONtA7MAku5792Z-dOFsuTQm8z9-saCo2mS81w58Lq-RAo1beg3UpcFVIC8ID07BhSrdGSWBClwR8SajrwsJHAOc7WAu6NJZCNwWUyncpFn4kIeoXuWgaa9o99xCrturA6yjxrnCYy1ZCvjNvDi2zjfIVk6auQWeSUUFFU9MxD1OElCZor_Qbwe7_hmnjK6yb-zrd-L5eCjzlztNRMDqdPMBzUQsvq0gBn8rFvIxUGgfQMmSu6T9mnRVKN8EjqGnWCmIxTMEbcV7kevPCCZxyc0H_F_vBTzQwPnEJD0SWd-IPrkniir2HhnyPe75D4oHw6Hxp_c7Ozy6vo3qKahbVPKoFxQeDyyHKCGWM8oAyQXmkDYrQu6huohrSzNEe001F-7lzsCPPlIbv7C7_qOA91Uqu_OU9XoOthSrxdW2jb8lxe2pY8j6aK-HQ6iW_A5xX-a8JtLBO8bUxEbDUOywUmhLf3BAfjrG8_yrWDnpcBG8WX1ryvrcBMuhWCdzBmlC7b7RqQu4)

[Accounting & Finance Applications Map](https://mermaid.ink/svg/eyJjb2RlIjoiJSV7aW5pdDogeydzZWN1cml0eUxldmVsJzogJ2xvb3NlJywgJ3RoZW1lJzonYmFzZSd9fSUlXG5ncmFwaCBMUlxuc3ViZ3JhcGggYWNjb3VudGluZyAmIGZpbmFuY2VcbkEoTkVXU1RBUilcbkIoRklOQU5DSUFMIE1PREVMKVxuQyhQUk9DT1JFKVxuRChDT05DVVIpXG5FKEJVSUxEUFJPKVxuRihZQVJESSlcbnN1YmdyYXBoIHJlcG9ydGluZ1xuRyhJTkZPUk1YTClcbkgoUE9XRVIgQkkpXG5lbmRcbkItLi0-fGJ1ZGdldHN8QVxuQy0uLi4uLT58aGFyZCBjb3N0IGJ1ZGdldHN8QlxuQy0uLi0-fHBheWFwcHN8RFxuRC0uLT58aW52b2ljZXN8QVxuQS0uLi0-fHBheW1lbnRzfENcbkQtLi0-fGNyZWRpdCBjYXJkc3xBXG5ELS4tPnxleHBsb3llZSBleHBlbnNlc3xBXG5FLS0-fGFwcHJvdmFsc3xBXG5BLS0tPnxwdXJjaGFzZSBvcmRlcnN8RVxuRi0uLT58bmV0IGluY29tZXxBXG5ELS4tPnxpbnZvaWNlc3xGXG5BLS0-fGRhdGF8R1xuQS0tPnxkYXRhfEhcbkMtLT58ZGF0YXxIXG5ILS0-fGFjdHVhbHN8QlxuZW5kIiwibWVybWFpZCI6bnVsbH0)

[Concur Invoice Playbook](https://wearestorybuilt.sharepoint.com/:b:/s/GlobalInformation/ESMl5wH47lZLhzx6dVbWATYB5hhERt2zZOATQxDIhxfoGA?e=p9Cxuc)

[User Training Recording](https://wearestorybuilt.sharepoint.com/:v:/s/GlobalInformation/EV0GOETKB0BJhB7Gw4K_HnQBeKX80vqkZdjMuTO4nV5pTw)

The following are opportunities for improvement that will be addressed in another phase of implementation:

- import approved invoices from Concur into Newstar
- import payment data from Newstar into Concur
- import vendors from Newstar into Concur
- import expense types from Newstar into Concur
- import projects/units from Newstar into Concur
- update approval workflows in Concur
- add reporting for managers
  
### `KR 3b` Establish new architecture for JV accounting in Newstar.

We established a new methodology and naming/coding convention for projects and confirmed consolidation functionality in Newstar (child entity into parent entity). The accounting team is still working through the transactions for new JV projects and closing out the old projects. Upon completion of the initial accounting transactions, we will adjust reporting and other systems for the new methodology and publish processes related to monthly consolidations.

[Newstar Structure - Company, Division, and Project Objects](https://wearestorybuilt.sharepoint.com/:w:/s/Systems/ESS3upP3oH9FqbAVNVqA8YsBCuIyZ8hQOu5c-vwVFZCZ3w?e=SJc0dn)

</details>

## Objective 4: *Construction*

*Reduce closing delays and back-end punch items by establishing company-wide system playbooks related to quality and increasing visibility into unit-specific construction schedules.*

<details  markdown="block">
  <summary>Key Results</summary>
  {: .text-delta }

### `KR 4a` Create documentation for Procore quality and safety tools.

We completed our review of the tools related to quality and safety (daily log, inspections, observations, punch list, incidents). 

A set of [process documents](https://wearestorybuilt.sharepoint.com/:f:/s/construction2/EvjusDRD9DNDiNSkQAo3WEsB9vlI1es9mTGpSOPqXRWt5w?e=NMKnTA) and a [summary presentation](https://wearestorybuilt.sharepoint.com/:p:/s/construction2/EQYo2ugGxwtIqJmANjKRq4kBk-56t2qGdPlUXElirFiKnA?e=4fBBYd) are the final deliverables for this series. We will continue to work through other tools and ultimately return to these documents for revision as processes and tools evolve.​ 

### `KR 4b` Integrate unit-specific milestone data from MS Project schedules.

We worked with the construction team at George to build a schedule template that allows us to parse individual unit milestones and integrate them into our other schedule datasets. The proof of concept was successful, but the current solution is fragile because it depends on a careful naming convention for schedule tasks. We will continue down this path and see if we can successfully reproduce the integration for another project schedule.  

</details>

## Objective 5: *Enablement*

*Increase technology user satisfaction by upgrading infrastructure, procurement, and branding.*

<details  markdown="block">
  <summary>Key Results</summary>
  {: .text-delta }

### `KR 5a` Upgrade Austin office audio/video infrastructure.

The Tango Conference room was upgraded with the Logitech Web Cam and Lenovo Thinksmart Core to improve in-office Teams meetings.

Once installed, the Frank video conferencing technology proved to be an effective upgrade for our main office that is well received and was easily integrated into our office tools.

### `KR 5b` Update tenant domain name.

The Sharepoint domain has been updated from pswrealestate to wearestorybuilt to better reflect our brand.

Updating the Sharepoint tenant domain name had little to no disruption and required less preparation and discovery than initially expected.

### `KR 5c` Assess new hardware procurement and provisioning partners.

Hardware procurement is being transitioned away from Terminal B to CDW to further establish consistency in new computer orders and imaging.

### `Other`

File and data backup systems were comprehensively tested. All Technology managed application licenses were reviewed for appropriate use and adjusted accordingly.

The phishing simulation training proved to be an effective way to gauge and encourage company digital security and awareness of potential digital threats.

The onboarding process both internal and external (Terminal B) remains in flux with many inefficient procedures preventing smooth transitions to our company for new hires. The People team struggles to find the most efficient and consistent way to notify Technology of new hires in a timely manner and provide ample time for software / hardware procurement as well as account creation and correspondence.

Without direct oversight and review, Terminal B consistently set up machines incorrectly or incompletely for new hires which had to be accommodated retroactively during their first week.

The application scrub of Bluebeam proved helpful and eye opening in regards to how our licensing had been handled before and what will be helpful in the future.

Procuring the ability to add / adjust Office 365 licenses and Active Directory accounts significantly elevated the ability to assist new and existing employees in a timely manner.

Things we can do better:

- Establishing a better habit of pushing on Terminal B for more prompt results, especially on more long-winded projects and tasks

- Concerning repetitive tasks (whether they be weekly / monthly / quarterly), scheduling that time beforehand to ensure consistency. Ex. – new hire check ins / invoices / payments / collaboration with external contacts / prioritizing tasks

</details>

### [Previous Quarters](retrospectives)


