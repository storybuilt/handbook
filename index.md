---
layout: default
title: Handbook Home
nav_order: 0
description: ""
permalink: /
---

# Handbook
{: .fs-9 }

This is how we run the company.
{: .fs-6 .fw-300 }

---

# :construction: UNDER CONSTRUCTION :construction:

# Handbook Culture

As a team scales, the need for documentation increases in parallel with the cost of not doing it. Said another way, implementing a documentation strategy becomes more difficult — yet more vital — as a company ages and matures.

There's a very real fear that committing to a handbook, and instilling a culture of documentation, is too tall a task to actually accomplish. The goal should not be to complete the handbook before ever announcing its existence to the company.

A common belief is that a company wiki can serve as a handbook, but the reality is that wikis do not scale.

>I think of it as brick laying. Every piece of information is a brick. At GitLab, there is a well-structured house, and everyone adds to that one house. Because we're pretty particular on how we build it, it has a strong foundation and we can build it very high.
>
>In every other company, they send the brick into the hands of people. Everyone is receiving bricks daily that they have to add to the house they're building internally. They forget things and things are unclear. A lot of context has to be created because there is no context around where to place the bricks.
>
>So, you can end up with a thousand houses that look quite different, that are all hanging a bit, and each time you add a brick to the top one pops out at the bottom. 

>*— GitLab co-founder and CEO Sid Sijbrandij*

# Guidelines

required/optional sections

documentation terminology (handbook, playbook, reference, SSoT, etc.)

link to sensitive/proprietary data

```mermaid!
graph LR
    1[handbook home]
    2[about us]
    3[company objectives]
    
    A[department]
    B[subdepartment]

    C[how we work]
    D[how to reach us]
    E[key performance indicators]
    F[objectives and key results]

    G[references]
    H[playbooks]
    I[policies]
    J[principles]

        C-->B
    A-...->C & D & E & F
    B-...->G & H & I & J
```

# Resources

[Almanac.io](https://almanac.io/home)

[GitLab Handbook](https://about.gitlab.com/handbook/)


# How to Update

submit docs in any form; updates by systems team

or self-serve update for more technical users via git repo

